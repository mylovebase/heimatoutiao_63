// 模块化--工程化
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import { Toast } from 'vant'

// 路由模块对象的创建及配置
const router = new VueRouter({
  routes: [
    {
      path: '/',
      redirect: { name: 'index' }
    },
    {
      name: 'index',
      path: '/index',
      component: () => import('../views/index.vue'),
      // 添加路由元信息配置
      meta: {
        active: localStorage.getItem('heimatoutiao_token_63') ? 1 : 0,
        height: 0
      }
    },
    {
      name: 'login',
      path: '/login',
      component: () => import('../views/login.vue')
    },
    {
      name: 'register',
      path: '/register',
      component: () => import('../views/register.vue')
    },
    {
      name: 'personal',
      // 因为个人中心页的数据获取，需要用户id，所以从登陆跳转到个人中心页的时候，传递用户id
      path: '/personal/:id',
      component: () => import('../views/personal.vue')
    },
    {
      name: 'editPersonal',
      // 路由配置时要不要传递参数，一般如何考虑？
      path: '/edit_profile/:id',
      component: () => import('../views/edit_profile.vue')
    },
    {
      name: 'postDetail',
      // 路由配置时要不要传递参数，一般如何考虑？接口文档明确告诉我们，获取文章详情需要文章id
      path: '/postDetail/:id',
      component: () => import('../views/postDetail.vue')
    },
    {
      name: 'myfollows',
      // 路由配置时要不要传递参数，一般如何考虑？接口文档明确告诉我们，获取用户关注列表不需要传递参数
      path: '/myfollows',
      component: () => import('../views/myfollows.vue')
    },
    {
      name: 'mystars',
      // 路由配置时要不要传递参数，一般如何考虑
      path: '/mystars',
      component: () => import('../views/mystars.vue')
    },
    {
      name: 'comment',
      // 路由配置时要不要传递参数，一般如何考虑？接口文档明确告诉我们，获取文章的评论列表需要传递参数(文章id)
      path: '/comment/:id',
      component: () => import('../views/comment.vue')
    },
    {
      name: 'cateManager',
      path: '/cateManager',
      component: () => import('../views/cateManager.vue')
    },
    {
      name: 'search',
      path: '/search',
      component: () => import('../views/search.vue')
    }
  ]
})

// 导航守卫
// to:跳转目标路由
// next:下一步操作，确保调用一次
// 设置一个需要登陆的  黑名单  或者 白名单
// 下面我们定义一个黑名称，里面包含着需要进行登陆验证的路由名称，后期如果跳转到目标路由名称在这个数组范围内，则需要进行登陆验证
let arr = ['personal', 'editPersonal']
router.beforeEach((to, from, next) => {
  if (arr.indexOf(to.name) !== -1) { // 说明这次的跳转需要验证是否登陆
    let token = localStorage.getItem('heimatoutiao_token_63')
    if (token) { // 登陆了
      next()
    } else { // 没有登陆就重定向到登陆页
      Toast.fail('未登陆，请先登陆')
      next({ name: 'login' })
    }
  } else {
    next()
  }
})

// 暴露
export default router