// 实现项目中axios的封装
import axios from 'axios'

import { Toast } from 'vant'

// 配置基准路径
axios.defaults.baseURL = 'http://157.122.54.189:9083'


// 添加请求拦截器---不要修改其它的代码
// 所有请求都会经过请求拦截器

// 拦截器只是相当于一个加工器，并不代替发请求
// config：相当于请求报文对象
axios.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么：看看有没有token啰，如果有通过请求头的方式传递token
  let token = localStorage.getItem('heimatoutiao_token_63')
  if (token) { // 判断是否有token,有，则
    // config.headers['Authorization'] = token
    config.headers.Authorization = token
  }

  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});


// 添加一个响应拦截器，处理  用户操作时 未登陆的情况
// 每一个响应都会经过响应拦截器
// response：就是本次请求从服务器端获取的响应内容
axios.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  // console.log(response);
  if (response.data.message === '用户信息验证失败') {
    Toast.fail('未登陆，请先登陆')
    // 传统方式进行跳转
    // location.hash = '#/login'
    location.href = '#/login?redirectUrl=' + location.href
  }

  return response;
}, function (error) {
  // 对响应错误做点什么
  return Promise.reject(error);
});

// 暴露
export default axios