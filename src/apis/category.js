// 这个文件主要封装与category栏目数据相关的操作

// 重点细节：一定要引入我们自己封装的axios,因为它才有基准路径的配置 
import axios from '../utils/request'

// 1. 获取所有栏目数据
export const getCatelist = () => {
  return axios({
    url: '/category'
  })
}
