// 这个文件主要封装与文件上传相关的操作

// 重点细节：一定要引入我们自己封装的axios,因为它才有基准路径的配置 
import axios from '../utils/request'

// 1. 实现文件上传
export const uploadFile = (data) => {
  return axios({
    method: 'post',
    url: '/upload',
    data
  })
}