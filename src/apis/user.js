// 这个文件主要封装与user数据相关的操作

// 重点细节：一定要引入我们自己封装的axios,因为它才有基准路径的配置 
import axios from '../utils/request'

// 1.用户登陆
export const userLogin = (data) => {
  // axios():返回的是一个promise对象
  return axios({
    // 注意，在axios中指定方式是method
    method: 'post',
    url: '/login',
    data
  })
}

// 2.用户注册
export const userRegister = (data) => {
  return axios({
    // 注意，在axios中指定方式是method
    method: 'post',
    url: '/register',
    data
  })
}

// 3.获取用户详情
// id:用户id
export const getUserInfo = (id) => {
  // 重点说明：参数的传递方式
  // url/:id  这种参数在url以固定格式拼接,在传递参数的时候不用再添加：了，不能使用?,或者使用params,query
  // 拼接的时候，注意不要随意加空格
  return axios({
    url: `/user/${id}`,
    // 传递token,键只能参照后台接口的说明
    // headers: { Authorization: localStorage.getItem('heimatoutiao_token_63') }
  })
}

// 4.编辑用户信息
// id:用户id
// data:用户相关信息
export const editUserInfo = (id, data) => {
  return axios({
    method: 'post',
    // id只能拼接，并且只能和url一起拼接
    url: `/user_update/${id}`,
    data
  })
}

// 5.关注用户
export const followUser = (id) => {
  return axios({
    url: `/user_follows/${id}`
  })
}

// 6.取消关注用户
export const unfollowUser = (id) => {
  return axios({
    url: `/user_unfollow/${id}`
  })
}

// 7.获取用户关注列表
export const getFollowList = () => {
  return axios({
    url: '/user_follows'
  })
}

// 8.获取用户收藏列表
export const getStarList = () => {
  return axios({
    url: '/user_star'
  })
}