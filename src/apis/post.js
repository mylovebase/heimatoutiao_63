// 这个文件主要封装与post新闻数据相关的操作

// 重点细节：一定要引入我们自己封装的axios,因为它才有基准路径的配置 
import axios from '../utils/request'


// 1.获取指定栏目的新闻数据
export const getPostList = (params) => {
  return axios({
    url: '/post',
    params
  })
}

// 2.根据文章id获取文章详情
export const getPostDeatil = (id) => {
  return axios({
    url: `/post/${id}`
  })
}

// 3.文章点赞和取消点赞
// id:文章id
export const likeThisPost = (id) => {
  return axios({
    url: `/post_like/${id}`
  })
}

// 4.文章收藏和取消收藏
// id:文章id
export const starThisPost = (id) => {
  return axios({
    url: `/post_star/${id}`
  })
}


// 5.获取文章评论数据
export const getPostComment = (id, params) => {
  return axios({
    url: `/post_comment/${id}`,
    params
  })
}

// 6.发表评论
export const publishComment = (id, data) => {
  return axios({
    method: 'post',
    url: `/post_comment/${id}`,
    data
  })
}

// 7.根据关键字搜索文章
export const searchPost = (params) => {
  return axios({
    url: '/post_search',
    params
  })
}