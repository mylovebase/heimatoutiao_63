import Vue from 'vue'
import App from './App.vue'

// 根据模块加载规则，引入一个目录会默认引入这个目录下面的index文件
import router from './router'


// 引入全局的重置样式文件
import './styles/reset.less'


import { Toast, Icon, Uploader, Dialog, Field, Search, ActionSheet, Tab, Tabs, List, PullRefresh, SwipeCell, Button } from 'vant';
// 当你use之后就会在Vue的prototype上挂载这个组件实例，以后在所有单文件组件中都能使用。以$开头  如  this.$toast
Vue
  .use(Toast) //一次只有use一个
  .use(Icon)
  .use(Uploader)
  .use(Dialog)
  .use(Field)
  .use(ActionSheet)
  .use(Tab)
  .use(Tabs)
  .use(List)
  .use(PullRefresh)
  .use(SwipeCell)
  .use(Button)
  .use(Search)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
